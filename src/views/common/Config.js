const httpUrl='http://192.168.240.23:9090';
const wsUrl='ws://192.168.240.23:9091';
export default {
    //后端接口路径
    loginAuth: httpUrl+'/api/login',
    k8sWorkflowCreate: httpUrl+'/api/k8s/workflow/create',
    k8sWorkflowDetail: httpUrl+'/api/k8s/workflow/detail',
    k8sWorkflowList: httpUrl+'/api/k8s/workflows',
    k8sWorkflowDel: httpUrl+'/api/k8s/workflow/del',
    k8sDeploymentList: httpUrl+'/api/k8s/deployments',
    k8sDeploymentDetail: httpUrl+'/api/k8s/deployment/detail',
    k8sDeploymentUpdate: httpUrl+'/api/k8s/deployment/update',
    k8sDeploymentScale: httpUrl+'/api/k8s/deployment/scale',
    k8sDeploymentRestart: httpUrl+'/api/k8s/deployment/restart',
    k8sDeploymentDel: httpUrl+'/api/k8s/deployment/del',
    k8sDeploymentCreate: httpUrl+'/api/k8s/deployment/create',
    k8sDeploymentNumNp: httpUrl+'/api/k8s/deployment/numnp',
    k8sPodList: httpUrl+'/api/k8s/pods',
    k8sPodDetail: httpUrl+'/api/k8s/pod/detail',
    k8sPodUpdate: httpUrl+'/api/k8s/pod/update',
    k8sPodDel: httpUrl+'/api/k8s/pod/del',
    k8sPodContainer: httpUrl+'/api/k8s/pod/container',
    k8sPodLog: httpUrl+'/api/k8s/pod/log',
    k8sPodNumNp: httpUrl+'/api/k8s/pod/numnp',
    k8sDaemonSetList: httpUrl+'/api/k8s/daemonsets',
    k8sDaemonSetDetail: httpUrl+'/api/k8s/daemonset/detail',
    k8sDaemonSetUpdate: httpUrl+'/api/k8s/daemonset/update',
    k8sDaemonSetDel: httpUrl+'/api/k8s/daemonset/del',
    k8sStatefulSetList: httpUrl+'/api/k8s/statefulsets',
    k8sStatefulSetDetail: httpUrl+'/api/k8s/daemonset/detail',
    k8sStatefulSetUpdate: httpUrl+'/api/k8s/daemonset/update',
    k8sStatefulSetDel: httpUrl+'/api/k8s/daemonset/del',
    k8sServiceList: httpUrl+'/api/k8s/services',
    k8sServiceDetail: httpUrl+'/api/k8s/service/detail',
    k8sServiceUpdate: httpUrl+'/api/k8s/service/update',
    k8sServiceDel: httpUrl+'/api/k8s/service/del',
    k8sServiceCreate: httpUrl+'/api/k8s/service/create',
    k8sIngressList: httpUrl+'/api/k8s/ingresses',
    k8sIngressDetail: httpUrl+'/api/k8s/ingress/detail',
    k8sIngressUpdate: httpUrl+'/api/k8s/ingress/update',
    k8sIngressDel: httpUrl+'/api/k8s/ingress/del',
    k8sIngressCreate: httpUrl+'/api/k8s/ingress/create',
    k8sConfigMapList: httpUrl+'/api/k8s/configmaps',
    k8sConfigMapDetail: httpUrl+'/api/k8s/configmap/detail',
    k8sConfigMapUpdate: httpUrl+'/api/k8s/configmap/update',
    k8sConfigMapDel: httpUrl+'/api/k8s/configmap/del',
    k8sSecretList: httpUrl+'/api/k8s/secrets',
    k8sSecretDetail: httpUrl+'/api/k8s/secret/detail',
    k8sSecretUpdate: httpUrl+'/api/k8s/secret/update',
    k8sSecretDel: httpUrl+'/api/k8s/secret/del',
    k8sPvcList: httpUrl+'/api/k8s/pvcs',
    k8sPvcDetail: httpUrl+'/api/k8s/pvc/detail',
    k8sPvcUpdate: httpUrl+'/api/k8s/pvc/update',
    k8sPvcDel: httpUrl+'/api/k8s/pvc/del',
    k8sNodeList: httpUrl+'/api/k8s/nodes',
    k8sNodeDetail: httpUrl+'/api/k8s/node/detail',
    k8sNamespaceList:  httpUrl+'/api/k8s/namespaces',
    k8sNamespaceDetail: httpUrl+'/api/k8s/namespace/detail',
    k8sNamespaceDel: httpUrl+'/api/k8s/namespace/del',
    k8sPvList: httpUrl+'/api/k8s/pvs',
    k8sPvDetail: httpUrl+'/api/k8s/pv/detail',
    k8sTerminalWs: wsUrl+'/ws',

    //编辑器配置
    cmOptions: {
        // 语言及语法模式
        mode: 'text/yaml',
        // 主题
        theme: 'idea',
        // 显示行数
        lineNumbers: true,
        smartIndent: true, //智能缩进
        indentUnit: 4, // 智能缩进单元长度为 4 个空格
        styleActiveLine: true, // 显示选中行的样式
        matchBrackets: true, //每当光标位于匹配的方括号旁边时，都会使其高亮显示
        readOnly: false,
        lineWrapping: true //自动换行
    }
}